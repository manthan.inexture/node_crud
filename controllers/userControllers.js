import userData from "../models/user.js";

//get all records of users
export const getUsers = async (req, res) => {

    try {
        const userdata = await userData.find();
        const userrecords = userdata.filter((element) => {
            // console.log("dfdf", element.deletedate)
            if (element.deletedate === null) {
                return element
            }
        })
        console.log(userrecords);
        res.status(200).json({ status: 200, message: "records found successfully", user: userrecords });
    } catch (error) {
        res.status(404).json({ error: error.message, status: 400, message: "user data not found" })
    }
}

//create uder
export const createUser = async (req, res) => {
    const userdata = new userData(req.body);
    let user = await userData.findOne({ username: req.body.username });
    if (!user) {
        try {
            await userdata.save();
            res.status(201).json({ status: 200, message: "user created successfully", userdata });
        } catch (error) {
            console.log(error.message);
            let errors = {};
            if (error.name === "ValidationError") {
                Object.keys(error.errors).forEach((key) => {
                    errors[key] = error.errors[key].message;
                });
            }
            return res.status(400).json(errors);
        }
    }
    else {
        return res.status(400).json({ status: 400, message: 'username already exist', user })
    }

}

export const deleteUser = async (req, res) => {
    let userId = req.params.id
    let user = await userData.findById(userId)
    if (user.deletedate === null) {
        try {
            await userData.findByIdAndUpdate(userId, { deletedate: new Date() })
            res.status(201).json({ status: 200, message: "user deleted successfully" });
        }
        catch (error) {
            res.status(400).json({ status: 400, message: "invalid user id" });
        }
    }
    else {
        res.status(400).json({ status: 400, message: "user already deleted" });
    }

}


export const updateUser = async (req, res) => {
    let id = req.params.id
    let userUpdatedData = req.body
    userUpdatedData.updatedate = new Date();
    try {
        await userData.findByIdAndUpdate(id, userUpdatedData)
        res.status(201).json({ status: 200, message: "user updated successfully", userData });
    }
    catch (error) {
        res.status(400).json({ status: 400, message: error.message });
    }
}