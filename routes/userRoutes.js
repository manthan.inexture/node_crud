import express from 'express';
const router = express.Router();
import * as userroute from '../controllers/index.js'
    
router.get('/getusers', userroute.getUsers); //get all users 
router.post('/createuser', userroute.createUser); //create new user
router.patch('/deleteuser/:id', userroute.deleteUser); //delete user by their ID
router.patch('/updateuser/:id', userroute.updateUser); // update user by their ID

export default router