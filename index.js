import express from "express";
import userRoutes from "./routes/userRoutes.js"
import dotenv from "dotenv";
dotenv.config({path:'config.env'})
import './connection/userConnection.js'

const port = process.env.PORT
const app = express();
app.use(express.json())

app.get('/', (req, res) => {
    res.send('Welcome to server')
})
app.use('/users', userRoutes);

app.listen(port, () => {
    console.log(`Your app listening at http://localhost:${port}`);
});

