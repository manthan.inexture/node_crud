import mongoose from "mongoose";
import { emailRegex } from "../helper/helper.js";

const userSchema = mongoose.Schema({
    firstname: {
        type: String,
        required: [true, "*required"]
    },
    lastname: {
        type: String,
        required: [true, "*required"]
    },
    email: {
        type: String,
        required: [true, "*required"],
        match: [emailRegex, 'Email is Invalid.']
    },
    username: {
        type: String,
        unique: [true, "already exist"],
        required: [true, "*required"]
    },
    dob: {
        type: String,
        match: [/^(0[1-9]|1[0-9]|2[0-9]|3[0-1])[- /.](0[1-9]|1[0-2])[- /.](20|21|22)\d\d$/, 'invalid date'],
        required: [true, "*required"]
    },
    createdate: {
        type: Date,
        default: new Date(),
    },
    updatedate: {
        type: Date,
        default: null,
    },
    deletedate: {
        type: Date,
        default: null,
    },
    status: {
        type: String,
        enum: [true, false],
        default: true,
    },
});
const userData = mongoose.model('userlist', userSchema)

export default userData;    